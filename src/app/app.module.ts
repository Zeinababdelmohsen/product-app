import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';

import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { CategoryService } from './services/category.service';
import { ProductService } from './services/product.service';
import { HttpClientModule } from '@angular/common/http';
import { ProductsFilterComponent } from './products-filter/products-filter.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ProductDetailsComponent } from './product-details/product-details.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ProductsFilterComponent,
    StarRatingComponent,
    NotFoundComponent,
    ProductDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent },
      { path: 'category/:id/:name', component: ProductDetailsComponent },
      { path: '**', component: NotFoundComponent }
    ])
  ],
  providers: [CategoryService, ProductService],
  bootstrap: [AppComponent]
})
export class AppModule {}
