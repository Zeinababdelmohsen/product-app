import { Component } from '@angular/core';
import { CategoryService } from 'app/services/category.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  categories: {};
  constructor(
    private categoryService: CategoryService,
    private router: Router
  ) {
    categoryService.getAllCategories().subscribe(response => {
      this.categories = response;
    });
  }

  /**
   * Navigate to the selected category
   * @param category object
   */

  onSelectedCategory(category) {
    this.router.navigate(['/category', category.id, category.name]);
  }
}
