import { Product } from './../models/product';
import { Component } from '@angular/core';
import { ProductService } from '../services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent {
  public products: Product[] = [];
  public filteredProducts: Product[] = [];
  public categoryId: number;
  public categoryName: String;
  public noProducts: Boolean = false;
  public APIName: String = '?categoryId=';

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private router: Router
  ) {
    // getting category id & name from route
    this.categoryId = this.route.snapshot.params['id'];
    this.categoryName = this.route.snapshot.params['name'];

    productService
      .getAllProducts(this.APIName, this.categoryId)
      .switchMap(products => {
        this.products = this.filteredProducts = products;
        return products;
      })
      .subscribe(
        () => {
          this.products = this.filteredProducts;
        },
        error => {
          console.log('ERROR:', error.message);
        }
      );
  }
  /**
   * All filter options colors, price and rating
   * @param filterOptions object
   */

  filter(filterOptions) {
    this.filteredProducts = this.products.filter(product => {
      let shouldBeReturned = true;
      if (filterOptions.rating) {
        shouldBeReturned = filterOptions.rating === product.rating;
      }
      if (filterOptions.colors) {
        if (filterOptions.colors.length > 0) {
          shouldBeReturned =
            shouldBeReturned && filterOptions.colors.includes(product.color);
        }
      }
      if (filterOptions.maxPrice) {
        shouldBeReturned =
          shouldBeReturned && product.price <= filterOptions.maxPrice;
      }
      if (filterOptions.minPrice) {
        shouldBeReturned =
          shouldBeReturned && product.price >= filterOptions.minPrice;
      }
      return shouldBeReturned;
    });
    this.noProducts = this.filteredProducts.length === 0 ? true : false;
  }

  /**
   * Searching for specific product inside selected category
   * @param event keyUpEvent
   */
  searchProducts(event) {
    this.filteredProducts = this.products.filter(product => {
      let shouldBeReturned = true;
      let productName = product.name.toLocaleLowerCase();
      let inputValue = event.target.value.toLocaleLowerCase();
      if (event.target) {
        if (productName.includes(inputValue)) {
          this.filteredProducts.splice(productName.indexOf(inputValue), 1);
        } else {
          shouldBeReturned = this.filteredProducts === this.products;
        }
      }
      return shouldBeReturned;
    });
    this.noProducts = this.filteredProducts.length === 0 ? true : false;
  }
}
