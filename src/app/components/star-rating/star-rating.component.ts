import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit {
  @Input('rating') rating;
  private starsList = {
    full: 0,
    empty: 0
  };
  constructor() {}

  ngOnInit() {
    this.starsList.full = this.rating;
    this.starsList.empty = 5 - this.rating;
  }

  /**
   * Get the iterator of the full stars
   * @returns array
   */
  fullStars() {
    return new Array(this.starsList.full);
  }

  /**
   * Get the iterator of the empty stars
   * @returns array
   */
  emptyStars() {
    return new Array(this.starsList.empty);
  }
}
