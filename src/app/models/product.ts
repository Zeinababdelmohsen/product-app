export interface Product {
  id: number;
  name: string;
  price: number;
  categoryId: number;
  image: string;
  currency: string;
  rating: number;
  releaseDate: string;
  color: string;
}
