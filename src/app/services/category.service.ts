import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Category } from '../models/category';

@Injectable()
export class CategoryService {
  private categoriesAPI = '';
  constructor(private http: HttpClient) {}

  // Getting all categories form API
  getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.categoriesAPI);
  }
}
