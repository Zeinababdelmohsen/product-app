import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Product } from '../models/product';

@Injectable()
export class ProductService {
  // products API link
  productsURL = '';
  constructor(private http: HttpClient) {}

  /**
   * Getting all the products of specific category
   * @param {number} id
   */
  getAllProducts(name, id): Observable<Product[]> {
    return this.http.get<Product[]>(this.productsURL + name + id);
  }
}
