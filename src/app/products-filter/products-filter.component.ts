import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';

@Component({
  selector: 'app-products-filter',
  templateUrl: './products-filter.component.html',
  styleUrls: ['./products-filter.component.scss']
})
export class ProductsFilterComponent implements OnChanges {
  public colors = [];
  public searchedColors = [];
  public rating = [1, 2, 3, 4, 5];
  public minPrice = 0;
  public maxPrice = 0;
  public noColors = false;
  public clearFiler = {};

  public filteredData = {
    rating: 0,
    colors: [],
    minPrice: 0,
    maxPrice: 0
  };

  @Output() filterUpdate: EventEmitter<any> = new EventEmitter<any>();
  @Input('products') products;

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    this.products.forEach(product => {
      if (
        !this.searchedColors.includes(product.color) &&
        !this.colors.includes(product.color)
      ) {
        this.searchedColors.push(product.color);
        this.colors.push(product.color);
      }
      if (this.minPrice === 0 || product.price < this.minPrice) {
        this.minPrice = product.price;
      }
      if (product.price > this.maxPrice) {
        this.maxPrice = product.price;
      }
    });

    this.filteredData.minPrice = this.minPrice;

    this.filteredData.maxPrice = this.maxPrice;
  }

  /**
   * Color filtration
   * @param event changeEvent
   */
  filterColors(event): void {
    const target = event.target;
    if (target.checked) {
      this.filteredData.colors.push(target.value);
    } else {
      this.filteredData.colors.splice(
        this.filteredData.colors.indexOf(target.value),
        1
      );
    }

    console.log(this.filteredData, target.value, target.checked);

    this.filterUpdate.emit(this.filteredData);
  }

  /**
   *  Rate filtration
   *  Getting the current selected value
   * @param event clickEvent
   * @param rateValue Number
   */

  filterRate(event, rateValue) {
    this.filteredData.rating = rateValue + 1;
    this.filterUpdate.emit(this.filteredData);
  }

  /**
   * @param event changeEvent
   */
  filterPrice(event, type) {
    let price = event.target.value;
    if (!price) {
      price = this[type + 'Price'];
      event.target.value = price;
    }
    this.filteredData[type + 'Price'] = price;
    this.filterUpdate.emit(this.filteredData);
  }

  /**
   * Searching for specific color
   * @param event keyUpEvent
   */

  searchColors(event) {
    this.searchedColors = this.colors.filter(color => {
      let shouldBeReturned = true;
      if (event.target) {
        if (color.includes(event.target.value)) {
          this.searchedColors.splice(color, 1);
        } else {
          shouldBeReturned = this.searchedColors === this.colors;
        }
      }
      return shouldBeReturned;
    });
  }

  /**
   * Clear filtration separately
   * @param filterType String
   */
  clearFilter(filterType) {
    if (filterType === 'colors') {
      this.filteredData.colors = [];
      document.querySelectorAll('.custom-control-input').forEach(input => {
        input['checked'] = false;
      });
    }
    if (filterType === 'rating') {
      this.filteredData.rating = 0;
    }
    if (filterType === 'price') {
      this.filteredData.minPrice = this.minPrice;
      this.filteredData.maxPrice = this.maxPrice;
    }
    this.filterUpdate.emit(this.filteredData);
  }
}
