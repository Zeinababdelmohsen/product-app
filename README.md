# Catalogue Front-task

Simple application for E-commerce store handling one module for displaying products.
The user can select a category to see its products.
User can filter products by **Colors**, **Average Rating**, **Range price** or combine two or more filters.
User can search for a specific product inside each category.

## Features

- Category list page.
- Products page displayed based on the selected category.
- Filtering with **Colors**, **Price range**, **Average rating**.
- Product search, any letters contained in the product name.

## Development Usage

This project was built with [Angular v.4](https://v4.angular.io/docs) and [Bootstrap v.4.2](https://getbootstrap.com/docs/4.2/getting-started/introduction) UI framework .

- Install the latest LTS version of Node.js from https://nodejs.org.
- Type `npm install` to install the dependencies as defined in the package.json file.
- Then run `ng serve -open` it will run and navigate automatically to `http://localhost:4200/` server.

## Architecture

- `src/app` holds all screens and services and routing for the application.

- `app/products-details` holds the products details for each category, inside `category.component.ts` all logic for the product filter emitted from `products-filter.component.ts` and product search function.

- `app/components` holds all public components for the application such as `navbar` , `rating` .

- `app/home` main page for the application contains categories list no need to be in separated file as its all simple module no large data to display.

- `app/models` contains two files for `category` and `product` Typescript Interfaces.

- `app/products-filter` contains all filter and functions for the application such as **Colors**, **Average Rating** and **Range price**.

- `app/services` using the Http object for Ajax calls along with RxJS observables for products and categories.

- `app/not-found` simple page for **Not founded pages** with wrong route.

- `assets/sass` holds all **SASS** files such as `helpers` contains all variables for the app.
